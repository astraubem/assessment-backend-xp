<?php
require_once('header.php');
?>

<!-- Main Content -->
<main class="content">
    
	<div class="header-list-page">
	  <h1 class="title">Página não Encontrada</h1>
	</div>

	<div class="error-404 not-found">
		<h3>Parece que essa página não existe</h3>
		<div class="error-404-amin">
			<img src="https://s3.amazonaws.com/hands.wtf/gifs/404.gif" style="display: block;">
		</div>
	</div>

</main>
<!-- Main Content -->

<?php
require_once('footer.php');
?>
